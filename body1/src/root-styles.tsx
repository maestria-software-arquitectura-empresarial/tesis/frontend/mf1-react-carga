import { CSSProperties } from 'react';

type PositionType = 'static' | 'relative' | 'absolute' | 'sticky' | 'fixed';
type TextAlignType = 'left' | 'right' | 'center' | 'justify' | 'initial' | 'inherit';

const styles: { [key: string]: CSSProperties } = {
  container: {
    fontFamily: 'Poppins, sans-serif',
    maxWidth: '600px',
    margin: '0 auto',
    padding: '20px',
    backgroundColor: '#ffffff',
    color: '#151940',
  },
  column: {
    marginLeft: '1%',
  },
  customDiv: {
    width: '150px',
    height: '234px',
    backgroundColor: '#F5F6FA',
    borderRadius: '10px',
    position: 'relative' as PositionType,
  },
  menu: {
    textAlign: 'center' as TextAlignType,
    marginTop: '20px',
    backgroundColor: '#D1D8F5',
    borderRadius: '10px',
    padding: '10px',
  },
};

export default styles;
