import useRootLogic from './root-logic';
import styles from './root-styles'; // Importa los estilos

const Root = (props: { name: string }) => {
  const {
    currentDate,
    formData,
    handleInputChange,
    handleFileUpload,
    handleFormSubmit,
    isFileUploaded,
  } = useRootLogic();

  return (
    <div className="container-fluid">
      <div className="container" style={styles.container}>
        <h3>Carga de Archivos</h3>
        <h5>Por favor, ingrese detalles de su Orden y adjunte la misma.</h5>

        <div className="row">
          <div className="col-12" style={{ backgroundColor: '#F5F6FA', borderRadius: '10px', padding: '2px', display: 'inline-block', alignItems: 'center', justifyContent: 'center', paddingLeft:'60px' }}>
            <br />
            <br />
            <h5>
            Por favor, selecciona un archivo
            </h5>
            <input type="file" id="fileInput" onChange={handleFileUpload} />
            {isFileUploaded ? null : <p style={{ color: 'red' }}>Por favor, carga un archivo.</p>}
          </div>
        </div>
        <div className='row'>
          <div className="col-8" style={{ paddingLeft: '25%', marginTop:'30px' }}>
            <input type="text" placeholder="Ingresa un nombre" id="nombre" name="nombre" value={formData.nombre} onChange={handleInputChange} style={{ marginBottom: '5%', width: '320px' }} />
            <br />
            <input type="text" placeholder="Ingresa una referencia" id="referencia" name="referencia" value={formData.referencia} onChange={handleInputChange} style={{ marginBottom: '5%', width: '320px' }} />
            <br />
            <input type="text" placeholder="Ingresa una descripcion" id="descripcion" name="descripcion" value={formData.descripcion} onChange={handleInputChange} style={{ marginBottom: '5%', width: '320px' }} />
            <br />
            <input type="text" placeholder="Ingresa un correo electronico" id="mail" name="mail" value={formData.mail} onChange={handleInputChange} style={{ marginBottom: '5%', width: '320px' }} />
            <br />
            <input type="text" placeholder="Ingresa un Número SMS" id="sms" name="sms" value={formData.sms} onChange={handleInputChange} style={{ marginBottom: '5%', width: '320px' }} />
            <br />
            <select name="banco" value={formData.banco} onChange={handleInputChange} style={{ marginBottom: '5%', width: '320px' }}>
              <option value="">Selecciona la institución de carga</option>
              <option value="Banco Central del Ecuador">Banco Central del Ecuador</option>
              <option value="Banco Boliviarano">Banco Boliviarano</option>
            </select>
            <br />
            <input  type="text" placeholder="Fecha de Carga" id="fechacarga" name="fechacarga" readOnly value={currentDate} style={{ marginBottom: '5%', width: '320px' }} />
            <button onClick={handleFormSubmit} style={{ marginBottom: '5%', width: '320px' }} className='btn-success'>Cargar</button>
          </div>
        </div>
      </div>
      <hr />

    </div>
  );
};

export default Root;
