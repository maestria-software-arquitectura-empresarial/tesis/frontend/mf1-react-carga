import React, { useState, useEffect } from 'react';
import { API_URL } from './config';

interface FormData {
    nombre: string;
    referencia: string;
    descripcion: string;
    transaccion: string;
    mail: string;
    sms: string;
    fechacarga: string;
    banco: string;
}

const useRootLogic = () => {
    const [currentDate, setCurrentDate] = useState<string>('');
    const [formData, setFormData] = useState<FormData>({
        nombre: '',
        referencia: '',
        transaccion: '',
        descripcion: '',
        mail: '',
        sms: '',
        banco: '',
        fechacarga: '',
    });
    const [isFileUploaded, setIsFileUploaded] = useState<boolean>(false);

    const updateCurrentDate = () => {
        const now = new Date();
        const formattedDate = `${now.getFullYear()}-${(now.getMonth() + 1)
            .toString()
            .padStart(2, '0')}-${now.getDate().toString().padStart(2, '0')}`;
        setCurrentDate(formattedDate);
    };

    useEffect(() => {
        updateCurrentDate();
    }, []);

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
        const { name, value } = e.target;
        setFormData((prevData) => ({
            ...prevData,
            [name]: value,
        }));
    };

    const handleFileUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files && e.target.files.length > 0) {
            setIsFileUploaded(true);
            const file = e.target.files[0];
            const reader = new FileReader();
            reader.onload = (e) => {
                const base64 = e.target?.result as string;
                const base64String2 = base64.split(',')[1];
                setFormData((prevFormData) => ({
                    ...prevFormData,
                    fechacarga: currentDate,
                    transaccion: base64String2,
                }));

            };
            reader.readAsDataURL(file);
        } else {
            setIsFileUploaded(false);
        }
    };

    const sendHttpRequest = async (url: string, body2: any): Promise<any> => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(body2)
        };
        try {
            const response = await fetch(url, requestOptions);
            const data = await response.json();
            console.log(data);
            return data;
        } catch (error) {
            console.error('Error en la solicitud HTTP:', error);
            throw error;
        }
    };

    const handleFormSubmit = async () => {
        const isFormValid =
            formData.nombre.trim() &&
            formData.referencia.trim() &&
            formData.descripcion.trim() &&
            formData.mail.trim() &&
            formData.sms.trim() &&
            formData.banco.trim() &&
            isFileUploaded;

        if (!isFormValid) {
            console.log('Por favor, llena los campos solicitados.');
            alert('Por favor, llena los campos solicitados.');
            return;
        }

        console.log(JSON.stringify(formData, null, 2));
        try {
            const responseData = await sendHttpRequest(API_URL, formData);
            console.log('Response from the service:', responseData);
            alert('Hemos procesado tu solicitud exitosamente.');
            // Resetear valores de campos
            setFormData({
                nombre: '',
                referencia: '',
                transaccion: '',
                descripcion: '',
                mail: '',
                sms: '',
                banco: '',
                fechacarga: '',
            });
            setIsFileUploaded(false);
            const fileInput = document.getElementById('fileInput') as HTMLInputElement;
            if (fileInput) {
                fileInput.value = '';
            }

        } catch (error) {
            console.error('Error sending request:', error);
            alert('Hemos tenido un error al procesar tu solicitud, por favor intenta nuevamente.');
        }

    };

    return {
        currentDate,
        formData,
        handleInputChange,
        handleFileUpload,
        handleFormSubmit,
        isFileUploaded,
    };
};

export default useRootLogic;
