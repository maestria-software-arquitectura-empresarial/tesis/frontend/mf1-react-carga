const { merge } = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-react-ts");

module.exports = (webpackConfigEnv, argv) => {
  
  const defaultConfig = singleSpaDefaults({
    orgName: "cuencadev",
    projectName: "body1",
    webpackConfigEnv,
    argv,
  });

  

  return merge(defaultConfig, {
    resolve: {
      fallback: {
        url: require.resolve('url/'),
        http: require.resolve('stream-http'), 
        buffer: require.resolve('buffer/'),
        
      },
    },
    devServer: {
      port: 9450, 
    },
  });
};
