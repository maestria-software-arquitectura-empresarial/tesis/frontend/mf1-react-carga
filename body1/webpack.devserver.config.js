module.exports = {
    mode: 'development',
    devServer: {
      headers: {
        "Content-Security-Policy": "default-src 'self'; connect-src *",
      },
    },
  };
  