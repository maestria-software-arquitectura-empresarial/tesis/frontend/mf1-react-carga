# Utilizamos una imagen de node como base
FROM node:latest

# Establecemos el directorio de trabajo en el contenedor
WORKDIR /app

# Copiamos los archivos de la aplicación al directorio de trabajo
COPY ./body1/package.json ./body1/package-lock.json ./

# Instalamos las dependencias del proyecto
RUN npm install

# Copiamos el resto de los archivos de la aplicación al directorio de trabajo
COPY ./body1/ ./

# Exponemos el puerto 9450 (el puerto por defecto de la aplicación React)
EXPOSE 9450

# Comando para iniciar la aplicación cuando se ejecute el contenedor
CMD ["npm", "start"]


# docker build -t mf1 .
# docker run -p 9450:9450 --name mf1_contenedor mf1
# mf1-react-carga
# body1 npm run build